# introduction


c console program that creates svg file with a model of Mandelbrot set. 

Based on the paper:
 
 M. Romera et al, Int. J. Bifurcation Chaos 13, 2279 (2003). Shrubs in the Mandelbrot Set Ordering 
 

Pdf:
* [original file](http:/www.tic.itefi.csic.es/gerardo/publica/Romera03.pdf)
* [local copy](Romera03.pdf)

#

 
# Algorithm  

line normal to the curve in polar equation
* [math24](https://www.math24.net/tangent-normal-lines/)
* [whitman](https://www.whitman.edu/mathematics/calculus_online/section10.02.html)
* 


## line normal to the circle


tangent to circle in the polar form   

$`z= r(t)`$  

[ is line](http://www.emathhelp.net/calculators/calculus-1/online-graphing-calculator/):  

$`y = x*tan(t)`$


Using [Maxima cas](./src/c.mac) and implicit differentiation :


```
m(x,y):=-x/y
m0:m(x0,y0)
tangent:y = m0*(x-x0)+y0
normal:y = (-(x-x0))/m0+y0

```
![Result](./images/circle.png)


## line normal to the ellipse

Using [Maxima cas](./src/e.mac) and implicit differentiation :


```
ellipse:x^2/a^2+y^2/b^2-1
m(x,y):=-(b^2*x)/(a^2*y)
m0:m(x0,y0)
tangent:y = m0*(x-x0)+y0
normal:y = (-(x-x0))/m0+y0

```
![Result](./images/ellipse.png)


## line normal to the cardioid

![](./images/normal2cardioid.png)


Image made with [emathhelp](http://www.emathhelp.net/calculators/calculus-1/online-graphing-calculator/) using:
* polar form of the cardioid : 4( 1 - cos(t))
* function cos takes input angle in radians so one have to change range from turns to radians : $`t = 2*pi*t`$ where t is the angle in turns 


Tangent to cardioid in polar form : 

$`z = 2a( 1 - cos(t))`$

is line : 

```
y = (x*(-1+2*cos(t))*sin(t)+(-2a+2a*cos(t))*sin(t))/(-1-cos(t)+2*cos(t)^2)
```

where t is in radians, changing from 0 to 2*pi



Using [Maxima cas](./src/cardioid.mac) and implicit differentiation :


```
cardioid:(x^2+y^2)^2+4*a*x*(x^2+y^2)+(-4)*a^2*y^2
m(x,y):=-((x+a)*y^2+x^3+3*a*x^2)/(y^3+(x^2+2*a*x-2*a^2)*y)
m0:m(x0,y0)
tangent:y = m0*(x-x0)+y0
normal:y = (-(x-x0))/m0+y0

```
![Result](./images/cardioid.png)




## shrub levels
 


# Results

# circles on the line

# circles on the circle 
* [image and src code in commons](https://commons.wikimedia.org/wiki/File:Shrub_model_of_Mandelbrot_set_60_10_labelled.png)
* my c code
  * [old: d.c](./src/d.c) 
  * [new: e.c ](./src/e.c)
* Haskell souce code by [Claude Heiland-Allen](http://mathr.co.uk/):  [circles on the circle ](./src/Romera03_circle.hs)  
 
 
![png](./images/20_20.png)


[Text output of the new code](./src/20_20_out.txt)


Image by [Claude Heiland-Allen](http://mathr.co.uk/)


![png](./images/Romera03_circle.png)


One can check component $`1-1/3-> 3 -1/3-> 9`$ hase more branches in Claude image  





# circles on the cardioid

to do ...


Image by [Claude Heiland-Allen](http://mathr.co.uk/)


![png](./images/Romera03_cardioid.png)

* Haskell souce code by [Claude Heiland-Allen](http://mathr.co.uk/): [circles on the cardioid ](./src/Romera03_cardioid.hs)  


# SVG 
* [svg check]( https://validator.w3.org/check)
* [svg test]( http://jsfiddle.net/j5ykdtgc/)



##  groups (optimisation)

Groups for 
* labels : text file 
* circles and lines : svg file
    
C program creates 2 files:
* svg fiele without labels
* [text file containing text group = labels](./src/20_20.txt)     

add labels (svg group ) to the svg file manually
    
## optimisation : limits : do not draw when
    - circle radius < 1.0 ???
    - text font-size 
    
    
## smooth curves other then circles and ellipses
* path
  * simple ( discrete points joined with lines) 
  * Bezier curves, cubic ( C)  and quadratic (Q)
  * arc = sections of circles or ellipses ( A)
    
    
    
# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)




# Git 
```git
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-shrub-model.git
git add d.c
git commit -m "Initial commit"
git push -u origin master
```

```git
git clone git@gitlab.com:adammajewski/color_gradient.git
```


Subdirectories:

```git
mkdir images
git add *.png
git mv  *.png ./images
git commit -m "move"
git push -u origin master
```

then change links to 


```
![](./images/n.png "description") 
```

to overwrite : 

```
gitm mv -f 
```



local repo : ~/c/mandel/shrub/shrub_git






