import Diagrams.Prelude hiding (star)
import Diagrams.Backend.Cairo.CmdLine (B, defaultMain)

import Data.Complex
import Data.Ratio

star :: [Integer] -> Diagram B
star [] = p2(0,0) ~~ p2(0,1)
star (q:qs) = scale (1 / fromInteger q) $ p2(0,0) ~~ p2(0,1) `atop` mconcat
  [ star qs
  # rotate (fromRational (p % q - 1/2) @@ turn)
  # translateY 1
  | p <- [ 1 .. q - 1 ]
  ]

shrub :: Integer -> Integer -> [Integer] -> Diagram B
shrub maxPeriod period decorations =
  (if period == 1 then cardioid else circle 1) `atop`
  (if null children
    then star (filter (/= 2) decorations) # scale s # translateY (4/3)
    else mempty) `atop`
  mconcat children
  where
    s =
      2 * 
      fromInteger (product (take 1 (reverse (dropWhile (== 2) decorations)))) *
      fromInteger (product                    decorations) ^ 2 /
      fromInteger (product $ dropWhile (== 2) decorations) ^ 2
    children =
      [ shrub maxPeriod (q * period) (q : decorations)
      # scale r
      # (if period == 1 then rotate a else translate v)
      # (if period == 1 then translate v else rotate a)
      | q <- [ 2 .. (maxPeriod + period - 1) `div` period ]
      , p <- [ 1 .. q - 1 ]
      , p `gcd` q == 1
      , let t = p % q
      , let r = if period == 1
                then sin (pi * fromRational t) / fromInteger q ^ 2
                else                         1 / fromInteger q ^ 2
      , let v = if period == 1
                then c2 (pointOnCardioid t + (r :+ 0) * normalToCardioid t)
                else r2 (0, 1 + r)
      , let a = if period == 1
                then angleOfNormalToCardioid t
                else fromRational (t - 1/2) @@ turn
      ]
    c2 (x :+ y) = r2 (-y, x)

pointOnCardioid t = u/2 * (1 - u/2)
  where
    u = cis a
    a = 2 * pi * fromRational t

{-
  -i d/dt P(t)
=
  -i d/dt e^{2 pi i t}/2 (1 - e^{2 pi i t}/2)
=
  -i ( pi i e^{2 pi i t} (1 - e^{2 pi i t}/2)
     + e^{2 pi i t}/2 (-pi i e^{2 pi i t}) )
=
  pi e^{2 pi i t} (1 - e^{2 pi i t}/2) - pi/2 (e^{2 pi i t})^2
=
  pi (u - u^2)
-}
normalToCardioid 0 = 0 :+ (-1)
normalToCardioid 1 = 0 :+ 1
normalToCardioid t = signum n
  where
    n = pi * (u - u^2)
    u = cis a
    a = 2 * pi * fromRational t

angleOfNormalToCardioid t = phase (normalToCardioid t) @@ rad

cardioid :: Diagram B
cardioid = cubicSpline False (map c2 points) # strokePath
  where
    points = [cusp] ++ [pointOnCardioid (t % 60) | t <- [1 .. 59]] ++ [cusp]
    cusp = 0.25
    c2 (x :+ y) = p2 (-y, x)

diagram :: Diagram B
diagram = shrub 60 1 [] # rotate (-1/4 @@ turn)
-- diagram = shrub (60 * 5*3*4) (5*3*4) [5,3,4]

main :: IO ()
main = defaultMain
  ( diagram
  # centerXY
  # frame 0.1
  # bg white
  # lw veryThin
  # lineCap LineCapRound
  )
