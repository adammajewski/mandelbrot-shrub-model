{-

Haskell code by Claude Heiland-Allen
"Recursion in Haskell is very easy, and the Diagrams library is powerful.

try: cabal install diagrams-cairo

or replace .Cairo. with .SVG. in the source code import (2nd line)


and run it with

./Romera03_circle -o out.png -w 1000

or for SVG backend

./Romera03 -o out.svg -w 1000

but Cairo PNG is much smaller output and faster than the SVG backend


-}

import Diagrams.Prelude hiding (star)
import Diagrams.Backend.Cairo.CmdLine (B, defaultMain)

import Data.Ratio ((%))

star :: [Integer] -> Diagram B
star [] = p2(0,0) ~~ p2(0,1)
star (q:qs) = scale (1 / fromInteger q) $ p2(0,0) ~~ p2(0,1) `atop` mconcat
  [ star qs
  # rotate (fromRational (p % q - 1/2) @@ turn)
  # translateY 1
  | p <- [ 1 .. q - 1 ]
  ]

shrub :: Integer -> Integer -> [Integer] -> Diagram B
shrub maxPeriod period decorations =
  circle 1 `atop`
  (if null children
    then star (filter (/= 2) decorations) # scale s # translateY (4/3)
    else mempty) `atop`
  mconcat children
  where
    s =
      2 * 
      fromInteger (product (take 1 (reverse (dropWhile (== 2) decorations)))) *
      fromInteger (product                    decorations) ^ 2 /
      fromInteger (product $ dropWhile (== 2) decorations) ^ 2
    children =
      [ shrub maxPeriod (q * period) (q : decorations)
      # scale r
      # translateY (1 + r)
      # rotate (fromRational (t - 1/2) @@ turn)
      | q <- [ 2 .. (maxPeriod + period - 1) `div` period ]
      , p <- [ 1 .. q - 1 ]
      , p `gcd` q == 1
      , let t = p % q
      , let r = 1 / fromInteger q ^ 2
      ]

main = defaultMain
  ( shrub 60 1 []
  # rotate (1/4 @@ turn)
  # centerXY
  # frame 0.1
  # bg white
  # lw veryThin
  # lineCap LineCapRound
  )
